from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
import time
from webdriver_manager.chrome import ChromeDriverManager


def test_creation_of_new_project():
    # Uruchomienie przeglądarki Chrome
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony TestArena
    browser.get('http://demo.testarena.pl/zaloguj')

    # Wpisanie login, hasła i kliknięcie login
    browser.find_element(By.CSS_SELECTOR, "#email").send_keys("administrator@testarena.pl")
    browser.find_element(By.CSS_SELECTOR, "#password").send_keys("sumXQQ72$L")
    browser.find_element(By.CSS_SELECTOR, "#login").click()

    # Otwarcie Admin Panel
    browser.find_element(By.CSS_SELECTOR, "[title=Administracja]").click()

    # Dodanie nowego projektu
    browser.find_element(By.CSS_SELECTOR,
                         "a.button_link[href='http://demo.testarena.pl/administration/add_project']").click()

    # Wpisanie nazwy
    browser.find_element(By.CSS_SELECTOR, "#name").send_keys("Project 4T WL")

    # Wpisanie prefixu
    browser.find_element(By.CSS_SELECTOR, "#prefix").send_keys("4TWL20")

    # Wpisanie opisu
    browser.find_element(By.CSS_SELECTOR, "#description").send_keys("This is my new project")

    # Dodanie projektu
    browser.find_element(By.CSS_SELECTOR, "[Name=save]").click()
    time.sleep(3)

    # Otwarcie sekcji Projekty
    browser.find_element(By.CSS_SELECTOR,
                         "a.activeMenu[href='http://demo.testarena.pl/administration/projects']").click()

    # Wyszukanie projektu po nazwie
    browser.find_element(By.CSS_SELECTOR, "#search").send_keys("4TWL20")
    time.sleep(3)
    browser.find_element(By.CSS_SELECTOR, "a#j_searchButton.icon_search").click()

    # Weryfikacja czy nowy projekt wyświetla się w wynikach wyszukiwania
    project_prefix = browser.find_element(By.CSS_SELECTOR, "td.t_number")
    assert project_prefix.text == "4TWL20"

    # Zamknięcie przeglądarki
    browser.close()
